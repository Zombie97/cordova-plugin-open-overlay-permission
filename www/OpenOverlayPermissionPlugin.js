var exec = require("cordova/exec");

module.exports.openOverlayPermission = function (options, successCallback, errorCallback) {
  exec(successCallback, errorCallback, "OpenOverlayPermissionPlugin", "openOverlayPermission", options);
};

module.exports.canDrawOverlays = function (options, successCallback, errorCallback) {
  exec(successCallback, errorCallback, "OpenOverlayPermissionPlugin", "canDrawOverlays", options);
};

module.exports.isMIUI = function (options, successCallback, errorCallback) {
  exec(successCallback, errorCallback, "OpenOverlayPermissionPlugin", "isMIUI", options);
};
