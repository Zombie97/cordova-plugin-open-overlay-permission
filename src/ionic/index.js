var plugin = function() {
    return OpenOverlayPermissionPlugin;
};

var openOverlayPermissionPlugin = /** @class */ (function() {
    function openOverlayPermissionPlugin() {}


    openOverlayPermissionPlugin.openOverlayPermission = function(options, successCallback, errorCallback) {
        var plu = plugin();
        return plu.openOverlayPermission.apply(plu, arguments);
    };
    openOverlayPermissionPlugin.canDrawOverlays = function(options, successCallback, errorCallback) {
        var plu = plugin();
        return plu.canDrawOverlays.apply(plu, arguments);
    };
    openOverlayPermissionPlugin.isMIUI = function(options, successCallback, errorCallback) {
        var plu = plugin();
        return plu.canDrawOverlays.apply(plu, arguments);
    };


    return openOverlayPermissionPlugin;
}());

export default openOverlayPermissionPlugin;
