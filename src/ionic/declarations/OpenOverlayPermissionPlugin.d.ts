
declare module 'cordova-plugin-open-overlay-permission' {

  export default class openOverlayPermissionPlugin {
    static openOverlayPermission(options: any, successCallback: (result: any) => void, errorCallback: (reason: any) => void);
    static canDrawOverlays(options: any, successCallback: (result: any) => void, errorCallback: (reason: any) => void);
    static isMIUI(options: any, successCallback: (result: any) => void, errorCallback: (reason: any) => void);

  }
}
